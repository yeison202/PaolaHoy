-- MySQL dump 10.13  Distrib 5.6.17, for Win32 (x86)
--
-- Host: localhost    Database: caso2
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `IdCliente` bigint(20) NOT NULL,
  `Nombre` varchar(30) DEFAULT NULL,
  `TelefonoFijo` bigint(20) DEFAULT NULL,
  `TelefonoCelular` bigint(20) DEFAULT NULL,
  `Correo` varchar(50) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdCliente`),
  KEY `ClienteEstado` (`IdEstado`),
  CONSTRAINT `cliente_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'Brayan Rojas',12345,111222333,'BrayanRojas@gmail.com',NULL),(2,'Julian Maldonado',67890,444555666,'JulianMaldonado@gmail.com',NULL),(3,'Miguel Zabaleta',12345,777888999,'MiguelZabaleta@gmail.com',NULL),(4,'Mary Torres',67890,111222333,'MaryTorres@gmail.com',NULL),(5,'Andrea Torres',12345,444555666,'AndreaTorres@gmail.com',NULL),(6,'Dayana Correa',67890,777888999,'DayanaCorrea@gmail.com',NULL),(7,'Julieth Nieto',12345,111222333,'JuliethNieto@gmail.com',NULL),(8,'Yeny Rincon',67890,444555666,'YenyRincon@gmail.com',NULL),(9,'Valentina Cortes',12345,777888999,'ValentinaCortes@gmail.com',NULL),(10,'Anna Quiñones',67890,111222333,'AnnaQuiñones@gmail.com',NULL),(11,'Brayan Rojas',12345,111222333,'BrayanRojas@gmail.com',NULL),(12,'Julian Maldonado',67890,444555666,'JulianMaldonado@gmail.com',NULL),(13,'Miguel Zabaleta',12345,777888999,'MiguelZabaleta@gmail.com',NULL),(14,'Mary Torres',67890,111222333,'MaryTorres@gmail.com',NULL),(15,'Andrea Torres',12345,444555666,'AndreaTorres@gmail.com',NULL),(16,'Dayana Correa',67890,777888999,'DayanaCorrea@gmail.com',NULL),(17,'Julieth Nieto',12345,111222333,'JuliethNieto@gmail.com',NULL),(18,'Yeny Rincon',67890,444555666,'YenyRincon@gmail.com',NULL),(19,'Valentina Cortes',12345,777888999,'ValentinaCortes@gmail.com',NULL),(20,'Anna Quiñones',67890,111222333,'AnnaQuiñones@gmail.com',NULL),(21,'Brayan Rojas',12345,111222333,'BrayanRojas@gmail.com',NULL),(22,'Julian Maldonado',67890,444555666,'JulianMaldonado@gmail.com',NULL),(23,'Miguel Zabaleta',12345,777888999,'MiguelZabaleta@gmail.com',NULL),(24,'Mary Torres',67890,111222333,'MaryTorres@gmail.com',NULL),(25,'Andrea Torres',12345,444555666,'AndreaTorres@gmail.com',NULL),(26,'Dayana Correa',67890,777888999,'DayanaCorrea@gmail.com',NULL),(27,'Julieth Nieto',12345,111222333,'JuliethNieto@gmail.com',NULL),(28,'Yeny Rincon',67890,444555666,'YenyRincon@gmail.com',NULL),(29,'Valentina Cortes',12345,777888999,'ValentinaCortes@gmail.com',NULL),(30,'Anna Quiñones',67890,111222333,'AnnaQuiñones@gmail.com',NULL),(31,'Brayan Rojas',12345,111222333,'BrayanRojas@gmail.com',NULL),(32,'Julian Maldonado',67890,444555666,'JulianMaldonado@gmail.com',NULL),(33,'Miguel Zabaleta',12345,777888999,'MiguelZabaleta@gmail.com',NULL),(34,'Mary Torres',67890,111222333,'MaryTorres@gmail.com',NULL),(35,'Andrea Torres',12345,444555666,'AndreaTorres@gmail.com',NULL),(36,'Dayana Correa',67890,777888999,'DayanaCorrea@gmail.com',NULL),(37,'Julieth Nieto',12345,111222333,'JuliethNieto@gmail.com',NULL),(38,'Yeny Rincon',67890,444555666,'YenyRincon@gmail.com',NULL),(39,'Valentina Cortes',12345,777888999,'ValentinaCortes@gmail.com',NULL),(40,'Anna Quiñones',67890,111222333,'AnnaQuiñones@gmail.com',NULL),(41,'Brayan Rojas',12345,111222333,'BrayanRojas@gmail.com',NULL),(42,'Julian Maldonado',67890,444555666,'JulianMaldonado@gmail.com',NULL),(43,'Miguel Zabaleta',12345,777888999,'MiguelZabaleta@gmail.com',NULL),(44,'Mary Torres',67890,111222333,'MaryTorres@gmail.com',NULL),(45,'Andrea Torres',12345,444555666,'AndreaTorres@gmail.com',NULL),(46,'Dayana Correa',67890,777888999,'DayanaCorrea@gmail.com',NULL),(47,'Julieth Nieto',12345,111222333,'JuliethNieto@gmail.com',NULL),(48,'Yeny Rincon',67890,444555666,'YenyRincon@gmail.com',NULL),(49,'Valentina Cortes',12345,777888999,'ValentinaCortes@gmail.com',NULL),(50,'Anna Quiñones',67890,111222333,'AnnaQuiñones@gmail.com',NULL);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalle`
--

DROP TABLE IF EXISTS `detalle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalle` (
  `IdDetalle` bigint(20) NOT NULL,
  `Ciudad` varchar(50) DEFAULT NULL,
  `IdEmpleado` bigint(20) DEFAULT NULL,
  `IdCliente` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdDetalle`),
  KEY `empleadodet` (`IdEmpleado`),
  KEY `clientedet` (`IdCliente`),
  CONSTRAINT `detalle_ibfk_1` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`IdEmpleado`),
  CONSTRAINT `detalle_ibfk_2` FOREIGN KEY (`IdCliente`) REFERENCES `cliente` (`IdCliente`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalle`
--

LOCK TABLES `detalle` WRITE;
/*!40000 ALTER TABLE `detalle` DISABLE KEYS */;
INSERT INTO `detalle` VALUES (1,'Bogota',NULL,NULL),(2,'Cartagena',NULL,NULL),(3,'Pereira',NULL,NULL),(4,'Cali',NULL,NULL),(5,'Buenaventura',NULL,NULL),(6,'Choco',NULL,NULL),(7,'Manizales',NULL,NULL),(8,'Santader',NULL,NULL),(9,'Santa Marta',NULL,NULL),(10,'Bogota',NULL,NULL),(12,'Medellin',NULL,NULL),(13,'Pereira',NULL,NULL),(14,'Cali',NULL,NULL),(15,'Buenaventura',NULL,NULL),(16,'Choco',NULL,NULL),(17,'Manizales',NULL,NULL),(18,'Santader',NULL,NULL),(19,'Santa Marta',NULL,NULL),(20,'Bogota',NULL,NULL),(21,'Medellin',NULL,NULL),(22,'Cartagena',NULL,NULL),(23,'Pereira',NULL,NULL),(24,'Cali',NULL,NULL),(25,'Buenaventura',NULL,NULL),(26,'Choco',NULL,NULL),(27,'Manizales',NULL,NULL),(28,'Santader',NULL,NULL),(29,'Santa Marta',NULL,NULL),(30,'Bogota',NULL,NULL),(31,'Medellin',NULL,NULL),(32,'Cartagena',NULL,NULL),(33,'Pereira',NULL,NULL),(34,'Cali',NULL,NULL),(35,'Buenaventura',NULL,NULL),(36,'Choco',NULL,NULL),(37,'Manizales',NULL,NULL),(38,'Santader',NULL,NULL),(39,'Santa Marta',NULL,NULL),(40,'Bogota',NULL,NULL),(41,'Medellin',NULL,NULL),(42,'Cartagena',NULL,NULL),(43,'Pereira',NULL,NULL),(44,'Cali',NULL,NULL),(45,'Buenaventura',NULL,NULL),(46,'Choco',NULL,NULL),(47,'Manizales',NULL,NULL),(48,'Santader',NULL,NULL),(49,'Santa Marta',NULL,NULL),(50,'Medellin',NULL,NULL);
/*!40000 ALTER TABLE `detalle` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallecfv`
--

DROP TABLE IF EXISTS `detallecfv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallecfv` (
  `IdDetalleCFV` bigint(20) NOT NULL,
  `Costo` bigint(20) DEFAULT NULL,
  `IdCliente` bigint(20) DEFAULT NULL,
  `IdFacturaVenta` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdDetalleCFV`),
  KEY `clientedetcfv` (`IdCliente`),
  KEY `clientefacv` (`IdFacturaVenta`),
  CONSTRAINT `detallecfv_ibfk_1` FOREIGN KEY (`IdCliente`) REFERENCES `cliente` (`IdCliente`),
  CONSTRAINT `detallecfv_ibfk_2` FOREIGN KEY (`IdFacturaVenta`) REFERENCES `facturaventa` (`IdFacturaVenta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallecfv`
--

LOCK TABLES `detallecfv` WRITE;
/*!40000 ALTER TABLE `detallecfv` DISABLE KEYS */;
INSERT INTO `detallecfv` VALUES (1,10000,NULL,NULL),(3,30000,NULL,NULL),(4,40000,NULL,NULL),(5,50000,NULL,NULL),(6,60000,NULL,NULL),(7,70000,NULL,NULL),(8,80000,NULL,NULL),(9,90000,NULL,NULL),(10,100000,NULL,NULL),(11,10000,NULL,NULL),(12,20000,NULL,NULL),(13,30000,NULL,NULL),(14,40000,NULL,NULL),(15,50000,NULL,NULL),(16,60000,NULL,NULL),(17,70000,NULL,NULL),(18,80000,NULL,NULL),(19,90000,NULL,NULL),(20,100000,NULL,NULL),(21,10000,NULL,NULL),(22,20000,NULL,NULL),(23,30000,NULL,NULL),(24,40000,NULL,NULL),(25,50000,NULL,NULL),(26,60000,NULL,NULL),(27,70000,NULL,NULL),(28,80000,NULL,NULL),(29,90000,NULL,NULL),(30,100000,NULL,NULL),(31,10000,NULL,NULL),(32,20000,NULL,NULL),(33,30000,NULL,NULL),(34,40000,NULL,NULL),(35,50000,NULL,NULL),(36,60000,NULL,NULL),(37,70000,NULL,NULL),(38,80000,NULL,NULL),(39,90000,NULL,NULL),(40,100000,NULL,NULL),(41,10000,NULL,NULL),(42,20000,NULL,NULL),(43,30000,NULL,NULL),(44,40000,NULL,NULL),(45,50000,NULL,NULL),(46,60000,NULL,NULL),(47,70000,NULL,NULL),(48,80000,NULL,NULL),(49,90000,NULL,NULL),(50,100000,NULL,NULL);
/*!40000 ALTER TABLE `detallecfv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `empleado`
--

DROP TABLE IF EXISTS `empleado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `empleado` (
  `IdEmpleado` bigint(20) NOT NULL,
  `Nombre` varchar(30) DEFAULT NULL,
  `Direccion` varchar(30) DEFAULT NULL,
  `Telefono` bigint(20) DEFAULT NULL,
  `FechaNacimiento` date DEFAULT NULL,
  `FechaVinculacion` date DEFAULT NULL,
  `Titulos` varchar(30) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdEmpleado`),
  KEY `EmpleadoEstado` (`IdEstado`),
  CONSTRAINT `empleado_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `empleado`
--

LOCK TABLES `empleado` WRITE;
/*!40000 ALTER TABLE `empleado` DISABLE KEYS */;
INSERT INTO `empleado` VALUES (1,'Brayan Rojas','av 12 n° 12-23',12345,'2016-04-05','2016-04-01','Bachiller',NULL),(2,'Brayan Olivares','av 23 n° 12-23',67890,'2015-04-05','2016-04-01','Tecnico',NULL),(3,'Julian Maldonado','av 34 n° 12-23',12345,'2016-04-05','2016-04-01','Universitario',NULL),(4,'Andrea martinez','av 45 n° 12-23',67890,'2012-04-05','2016-04-01','Doctorado',NULL),(5,'Luis Lopez','av 56 n° 12-23',12345,'2011-04-05','2016-04-01','Bachiller',NULL),(6,'Yeison Trijillo','av 67 n° 12-23',67890,'2016-04-05','2016-04-01','Tecnico',NULL),(7,'Dayana Correa','av 12 n° 12-23',12345,'2015-04-05','2016-04-01','Universitario',NULL),(8,'Hector Torres','av 23 n° 12-23',67890,'2014-04-05','2016-04-01','Doctorado',NULL),(9,'Gladys Torres','av 34 n° 12-23',12345,'2015-04-05','2016-04-01','Bachiller',NULL),(10,'Sara Bernal','av 56 n° 12-23',67890,'2016-04-05','2016-04-01','Tecnico',NULL),(11,'Brayan Rojas','av 12 n° 12-23',12345,'2016-04-05','2016-04-01','Bachiller',NULL),(12,'Brayan Olivares','av 23 n° 12-23',67890,'2015-04-05','2016-04-01','Tecnico',NULL),(13,'Julian Maldonado','av 34 n° 12-23',12345,'2016-04-05','2016-04-01','Universitario',NULL),(14,'Andrea martinez','av 45 n° 12-23',67890,'2012-04-05','2016-04-01','Doctorado',NULL),(15,'Luis Lopez','av 56 n° 12-23',12345,'2011-04-05','2016-04-01','Bachiller',NULL),(16,'Yeison Trijillo','av 67 n° 12-23',67890,'2016-04-05','2016-04-01','Tecnico',NULL),(17,'Dayana Correa','av 12 n° 12-23',12345,'2015-04-05','2016-04-01','Universitario',NULL),(18,'Hector Torres','av 23 n° 12-23',67890,'2014-04-05','2016-04-01','Doctorado',NULL),(19,'Gladys Torres','av 34 n° 12-23',12345,'2015-04-05','2016-04-01','Bachiller',NULL),(20,'Sara Bernal','av 56 n° 12-23',67890,'2016-04-05','2016-04-01','Tecnico',NULL),(21,'Brayan Rojas','av 12 n° 12-23',12345,'2016-04-05','2016-04-01','Bachiller',NULL),(22,'Brayan Olivares','av 23 n° 12-23',67890,'2015-04-05','2016-04-01','Tecnico',NULL),(23,'Julian Maldonado','av 34 n° 12-23',12345,'2016-04-05','2016-04-01','Universitario',NULL),(24,'Andrea martinez','av 45 n° 12-23',67890,'2012-04-05','2016-04-01','Doctorado',NULL),(25,'Luis Lopez','av 56 n° 12-23',12345,'2011-04-05','2016-04-01','Bachiller',NULL),(26,'Yeison Trijillo','av 67 n° 12-23',67890,'2016-04-05','2016-04-01','Tecnico',NULL),(27,'Dayana Correa','av 12 n° 12-23',12345,'2015-04-05','2016-04-01','Universitario',NULL),(28,'Hector Torres','av 23 n° 12-23',67890,'2014-04-05','2016-04-01','Doctorado',NULL),(29,'Gladys Torres','av 34 n° 12-23',12345,'2015-04-05','2016-04-01','Bachiller',NULL),(30,'Sara Bernal','av 56 n° 12-23',67890,'2016-04-05','2016-04-01','Tecnico',NULL),(31,'Brayan Rojas','av 12 n° 12-23',12345,'2016-04-05','2016-04-01','Bachiller',NULL),(32,'Brayan Olivares','av 23 n° 12-23',67890,'2015-04-05','2016-04-01','Tecnico',NULL),(33,'Julian Maldonado','av 34 n° 12-23',12345,'2016-04-05','2016-04-01','Universitario',NULL),(34,'Andrea martinez','av 45 n° 12-23',67890,'2012-04-05','2016-04-01','Doctorado',NULL),(35,'Luis Lopez','av 56 n° 12-23',12345,'2011-04-05','2016-04-01','Bachiller',NULL),(36,'Yeison Trijillo','av 67 n° 12-23',67890,'2016-04-05','2016-04-01','Tecnico',NULL),(37,'Dayana Correa','av 12 n° 12-23',12345,'2015-04-05','2016-04-01','Universitario',NULL),(38,'Hector Torres','av 23 n° 12-23',67890,'2014-04-05','2016-04-01','Doctorado',NULL),(39,'Gladys Torres','av 34 n° 12-23',12345,'2015-04-05','2016-04-01','Bachiller',NULL),(40,'Sara Bernal','av 56 n° 12-23',67890,'2016-04-05','2016-04-01','Tecnico',NULL),(41,'Brayan Rojas','av 12 n° 12-23',12345,'2016-04-05','2016-04-01','Bachiller',NULL),(42,'Brayan Olivares','av 23 n° 12-23',67890,'2015-04-05','2016-04-01','Tecnico',NULL),(43,'Julian Maldonado','av 34 n° 12-23',12345,'2016-04-05','2016-04-01','Universitario',NULL),(44,'Andrea martinez','av 45 n° 12-23',67890,'2012-04-05','2016-04-01','Doctorado',NULL),(45,'Luis Lopez','av 56 n° 12-23',12345,'2011-04-05','2016-04-01','Bachiller',NULL),(46,'Yeison Trijillo','av 67 n° 12-23',67890,'2016-04-05','2016-04-01','Tecnico',NULL),(47,'Dayana Correa','av 12 n° 12-23',12345,'2015-04-05','2016-04-01','Universitario',NULL),(48,'Hector Torres','av 23 n° 12-23',67890,'2014-04-05','2016-04-01','Doctorado',NULL),(49,'Gladys Torres','av 34 n° 12-23',12345,'2015-04-05','2016-04-01','Bachiller',NULL),(50,'Sara Bernal','av 56 n° 12-23',67890,'2016-04-05','2016-04-01','Tecnico',NULL);
/*!40000 ALTER TABLE `empleado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estado`
--

DROP TABLE IF EXISTS `estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estado` (
  `IdEstado` bigint(20) NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`IdEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estado`
--

LOCK TABLES `estado` WRITE;
/*!40000 ALTER TABLE `estado` DISABLE KEYS */;
INSERT INTO `estado` VALUES (1,'Activo'),(2,'Inactivo'),(3,'Activo'),(4,'Inactivo'),(5,'Activo'),(6,'Inactivo'),(7,'Activo'),(8,'Inactivo'),(9,'Activo'),(10,'Inactivo'),(11,'Activo'),(12,'Inactivo'),(13,'Activo'),(14,'Inactivo'),(15,'Activo'),(16,'Inactivo'),(17,'Activo'),(18,'Inactivo'),(19,'Activo'),(20,'Inactivo'),(21,'Activo'),(22,'Inactivo'),(23,'Activo'),(24,'Inactivo'),(25,'Activo'),(26,'Inactivo'),(27,'Activo'),(28,'Inactivo'),(29,'Activo'),(30,'Inactivo'),(31,'Activo'),(32,'Inactivo'),(33,'Activo'),(34,'Inactivo'),(35,'Activo'),(36,'Inactivo'),(37,'Activo'),(38,'Inactivo'),(39,'Activo'),(40,'Inactivo'),(41,'Activo'),(42,'Inactivo'),(43,'Activo'),(44,'Inactivo'),(45,'Activo'),(46,'Inactivo'),(47,'Activo'),(48,'Inactivo'),(49,'Activo'),(50,'Inactivo');
/*!40000 ALTER TABLE `estado` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturacompra`
--

DROP TABLE IF EXISTS `facturacompra`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturacompra` (
  `IdFacturaVenta` bigint(20) NOT NULL,
  `TipoMaterial` varchar(50) DEFAULT NULL,
  `Cantidad` bigint(20) DEFAULT NULL,
  `NombreMaterial` varchar(50) DEFAULT NULL,
  `IdProveedor` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdFacturaVenta`),
  KEY `proveedorFacCom` (`IdProveedor`),
  CONSTRAINT `facturacompra_ibfk_1` FOREIGN KEY (`IdProveedor`) REFERENCES `proveedor` (`IdProveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturacompra`
--

LOCK TABLES `facturacompra` WRITE;
/*!40000 ALTER TABLE `facturacompra` DISABLE KEYS */;
INSERT INTO `facturacompra` VALUES (1,'Cemento',100,'Cemento',NULL),(2,'Ladrillo',200,'Ladrillo',NULL),(3,'Estuco',300,'Estuco',NULL),(4,'Pintura',400,'Cemento',NULL),(5,'Palas',500,'Pintura',NULL),(6,'Picas',600,'Picas',NULL),(7,'Maya',700,'Maya',NULL),(8,'Vallas',800,'Vallas',NULL),(9,'Valdosas',900,'Valdosas',NULL),(10,'Estuco',1000,'Estuco',NULL),(11,'Cemento',100,'Cemento',NULL),(12,'Ladrillo',200,'Ladrillo',NULL),(13,'Estuco',300,'Estuco',NULL),(14,'Pintura',400,'Cemento',NULL),(15,'Palas',500,'Pintura',NULL),(16,'Picas',600,'Picas',NULL),(17,'Maya',700,'Maya',NULL),(18,'Vallas',800,'Vallas',NULL),(19,'Valdosas',900,'Valdosas',NULL),(20,'Estuco',1000,'Estuco',NULL),(21,'Cemento',100,'Cemento',NULL),(22,'Ladrillo',200,'Ladrillo',NULL),(23,'Estuco',300,'Estuco',NULL),(24,'Pintura',400,'Cemento',NULL),(25,'Palas',500,'Pintura',NULL),(26,'Picas',600,'Picas',NULL),(27,'Maya',700,'Maya',NULL),(28,'Vallas',800,'Vallas',NULL),(29,'Valdosas',900,'Valdosas',NULL),(30,'Estuco',1000,'Estuco',NULL),(31,'Cemento',100,'Cemento',NULL),(32,'Ladrillo',200,'Ladrillo',NULL),(33,'Estuco',300,'Estuco',NULL),(34,'Pintura',400,'Cemento',NULL),(35,'Palas',500,'Pintura',NULL),(36,'Picas',600,'Picas',NULL),(37,'Maya',700,'Maya',NULL),(38,'Vallas',800,'Vallas',NULL),(39,'Valdosas',900,'Valdosas',NULL),(40,'Estuco',1000,'Estuco',NULL),(41,'Cemento',100,'Cemento',NULL),(42,'Ladrillo',200,'Ladrillo',NULL),(43,'Estuco',300,'Estuco',NULL),(44,'Pintura',400,'Cemento',NULL),(45,'Palas',500,'Pintura',NULL),(46,'Picas',600,'Picas',NULL),(47,'Maya',700,'Maya',NULL),(48,'Vallas',800,'Vallas',NULL),(49,'Valdosas',900,'Valdosas',NULL),(50,'Estuco',1000,'Estuco',NULL);
/*!40000 ALTER TABLE `facturacompra` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facturaventa`
--

DROP TABLE IF EXISTS `facturaventa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facturaventa` (
  `IdFacturaVenta` bigint(20) NOT NULL,
  `Nombre` varchar(30) DEFAULT NULL,
  `Direccion` varchar(30) DEFAULT NULL,
  `Telefono` bigint(20) DEFAULT NULL,
  `NombreProducto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`IdFacturaVenta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facturaventa`
--

LOCK TABLES `facturaventa` WRITE;
/*!40000 ALTER TABLE `facturaventa` DISABLE KEYS */;
INSERT INTO `facturaventa` VALUES (1,'Brayan Rojas','av 12 n°12-34',12345,'Cemento'),(2,'Gladys Torres','av 34 n°12-34',12345,'Valdosas'),(3,'Andrea Matines','av 45 n°12-34',67890,'Pintura'),(4,'Brayan Olivares','av 56 n°12-34',12345,'Estuco'),(5,'Jason Rodriguez','av 67 n°12-34',67890,'Pico'),(6,'Julian Maldonado','av 12 n°12-34',12345,'Palas'),(7,'Yeison Trujillo','av 23 n°12-34',67890,'Vallas'),(8,'Santiago Angulo','av 34 n°12-34',12345,'Estuco'),(9,'Felipe Cortes','av 45 n°12-34',67890,'Cemento'),(10,'Brayan Rojas','av 12 n°12-34',12345,'Cemento'),(11,'Andres Parra','av 23 n°12-34',67890,'Maya'),(12,'Gladys Torres','av 34 n°12-34',12345,'Valdosas'),(13,'Andrea Matines','av 45 n°12-34',67890,'Pintura'),(14,'Brayan Olivares','av 56 n°12-34',12345,'Estuco'),(15,'Jason Rodriguez','av 67 n°12-34',67890,'Pico'),(16,'Julian Maldonado','av 12 n°12-34',12345,'Palas'),(17,'Yeison Trujillo','av 23 n°12-34',67890,'Vallas'),(18,'Santiago Angulo','av 34 n°12-34',12345,'Estuco'),(19,'Felipe Cortes','av 45 n°12-34',67890,'Cemento'),(20,'Brayan Rojas','av 12 n°12-34',12345,'Cemento'),(21,'Andres Parra','av 23 n°12-34',67890,'Maya'),(22,'Gladys Torres','av 34 n°12-34',12345,'Valdosas'),(23,'Andrea Matines','av 45 n°12-34',67890,'Pintura'),(24,'Brayan Olivares','av 56 n°12-34',12345,'Estuco'),(25,'Jason Rodriguez','av 67 n°12-34',67890,'Pico'),(26,'Julian Maldonado','av 12 n°12-34',12345,'Palas'),(27,'Yeison Trujillo','av 23 n°12-34',67890,'Vallas'),(28,'Santiago Angulo','av 34 n°12-34',12345,'Estuco'),(29,'Felipe Cortes','av 45 n°12-34',67890,'Cemento'),(30,'Brayan Rojas','av 12 n°12-34',12345,'Cemento'),(31,'Andres Parra','av 23 n°12-34',67890,'Maya'),(32,'Gladys Torres','av 34 n°12-34',12345,'Valdosas'),(33,'Andrea Matines','av 45 n°12-34',67890,'Pintura'),(34,'Brayan Olivares','av 56 n°12-34',12345,'Estuco'),(35,'Jason Rodriguez','av 67 n°12-34',67890,'Pico'),(36,'Julian Maldonado','av 12 n°12-34',12345,'Palas'),(37,'Yeison Trujillo','av 23 n°12-34',67890,'Vallas'),(38,'Santiago Angulo','av 34 n°12-34',12345,'Estuco'),(39,'Felipe Cortes','av 45 n°12-34',67890,'Cemento'),(40,'Brayan Rojas','av 12 n°12-34',12345,'Cemento'),(41,'Andres Parra','av 23 n°12-34',67890,'Maya'),(42,'Gladys Torres','av 34 n°12-34',12345,'Valdosas'),(43,'Andrea Matines','av 45 n°12-34',67890,'Pintura'),(44,'Brayan Olivares','av 56 n°12-34',12345,'Estuco'),(45,'Jason Rodriguez','av 67 n°12-34',67890,'Pico'),(46,'Julian Maldonado','av 12 n°12-34',12345,'Palas'),(47,'Yeison Trujillo','av 23 n°12-34',67890,'Vallas'),(48,'Santiago Angulo','av 34 n°12-34',12345,'Estuco'),(49,'Felipe Cortes','av 45 n°12-34',67890,'Cemento'),(50,'Brayan Rojas','av 12 n°12-34',12345,'Cemento');
/*!40000 ALTER TABLE `facturaventa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inventariomaterial`
--

DROP TABLE IF EXISTS `inventariomaterial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inventariomaterial` (
  `IdInventarioMaterial` bigint(20) NOT NULL,
  `NombreProducto` varchar(50) DEFAULT NULL,
  `Cantidad` bigint(20) DEFAULT NULL,
  `TipoProducto` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`IdInventarioMaterial`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inventariomaterial`
--

LOCK TABLES `inventariomaterial` WRITE;
/*!40000 ALTER TABLE `inventariomaterial` DISABLE KEYS */;
INSERT INTO `inventariomaterial` VALUES (1,'Cemento',100,'Cemento'),(2,'Valla',200,'valla'),(3,'Maya',300,'maya'),(4,'Pintura',400,'Pintura'),(5,'Estuco',500,'estuco'),(6,'Picas',600,'picas'),(7,'Palas',700,'palas'),(8,'Valdosas',800,'valdosas'),(9,'estuco',900,'estuco'),(10,'Valla',1000,'valla'),(11,'Cemento',100,'Cemento'),(12,'Valla',200,'valla'),(13,'Maya',300,'maya'),(14,'Pintura',400,'Pintura'),(15,'Estuco',500,'estuco'),(16,'Picas',600,'picas'),(17,'Palas',700,'palas'),(18,'Valdosas',800,'valdosas'),(19,'estuco',900,'estuco'),(20,'Valla',1000,'valla'),(21,'Cemento',100,'Cemento'),(22,'Valla',200,'valla'),(23,'Maya',300,'maya'),(24,'Pintura',400,'Pintura'),(25,'Estuco',500,'estuco'),(26,'Picas',600,'picas'),(27,'Palas',700,'palas'),(28,'Valdosas',800,'valdosas'),(29,'estuco',900,'estuco'),(30,'Valla',1000,'valla'),(31,'Cemento',100,'Cemento'),(32,'Valla',200,'valla'),(33,'Maya',300,'maya'),(34,'Pintura',400,'Pintura'),(35,'Estuco',500,'estuco'),(36,'Picas',600,'picas'),(37,'Palas',700,'palas'),(38,'Valdosas',800,'valdosas'),(39,'estuco',900,'estuco'),(40,'Valla',1000,'valla'),(41,'Cemento',100,'Cemento'),(42,'Valla',200,'valla'),(43,'Maya',300,'maya'),(44,'Pintura',400,'Pintura'),(45,'Estuco',500,'estuco'),(46,'Picas',600,'picas'),(47,'Palas',700,'palas'),(48,'Valdosas',800,'valdosas'),(49,'estuco',900,'estuco'),(50,'Valla',1000,'valla');
/*!40000 ALTER TABLE `inventariomaterial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `material`
--

DROP TABLE IF EXISTS `material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `material` (
  `IdMaterial` bigint(20) NOT NULL,
  `Descripcion` varchar(50) DEFAULT NULL,
  `Unidades` bigint(20) DEFAULT NULL,
  `Existencias` bigint(20) DEFAULT NULL,
  `StockMinimo` bigint(20) DEFAULT NULL,
  `IdInventarioMaterial` bigint(20) DEFAULT NULL,
  `IdProveedor` bigint(20) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdMaterial`),
  KEY `mateInvenM` (`IdInventarioMaterial`),
  KEY `proveedormat` (`IdProveedor`),
  KEY `MaterialEstado` (`IdEstado`),
  CONSTRAINT `material_ibfk_3` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`),
  CONSTRAINT `material_ibfk_1` FOREIGN KEY (`IdInventarioMaterial`) REFERENCES `inventariomaterial` (`IdInventarioMaterial`),
  CONSTRAINT `material_ibfk_2` FOREIGN KEY (`IdProveedor`) REFERENCES `proveedor` (`IdProveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `material`
--

LOCK TABLES `material` WRITE;
/*!40000 ALTER TABLE `material` DISABLE KEYS */;
INSERT INTO `material` VALUES (1,'Cemento',100,50,500,NULL,NULL,NULL),(2,'Vaya',200,60,700,NULL,NULL,NULL),(3,'Pintura',300,70,800,NULL,NULL,NULL),(4,'Estuco',400,80,900,NULL,NULL,NULL),(5,'palas',500,90,1000,NULL,NULL,NULL),(6,'Picos',600,100,2000,NULL,NULL,NULL),(7,'Valdosas',700,200,3000,NULL,NULL,NULL),(8,'Maya',800,300,4000,NULL,NULL,NULL),(9,'Ladrillo',900,400,5000,NULL,NULL,NULL),(10,'Carretilla',1000,500,6000,NULL,NULL,NULL),(11,'Cemento',100,50,600,NULL,NULL,NULL),(12,'Vaya',200,60,700,NULL,NULL,NULL),(13,'Pintura',300,70,800,NULL,NULL,NULL),(14,'Estuco',400,80,900,NULL,NULL,NULL),(15,'palas',500,90,1000,NULL,NULL,NULL),(16,'Picos',600,100,2000,NULL,NULL,NULL),(17,'Valdosas',700,200,3000,NULL,NULL,NULL),(18,'Maya',800,300,4000,NULL,NULL,NULL),(19,'Ladrillo',900,400,5000,NULL,NULL,NULL),(20,'Carretilla',1000,500,6000,NULL,NULL,NULL),(21,'Cemento',100,50,600,NULL,NULL,NULL),(22,'Vaya',200,60,700,NULL,NULL,NULL),(23,'Pintura',300,70,800,NULL,NULL,NULL),(24,'Estuco',400,80,900,NULL,NULL,NULL),(25,'palas',500,90,1000,NULL,NULL,NULL),(26,'Picos',600,100,2000,NULL,NULL,NULL),(27,'Valdosas',700,200,3000,NULL,NULL,NULL),(28,'Maya',800,300,4000,NULL,NULL,NULL),(29,'Ladrillo',900,400,5000,NULL,NULL,NULL),(30,'Carretilla',1000,500,6000,NULL,NULL,NULL),(31,'Cemento',100,50,600,NULL,NULL,NULL),(32,'Vaya',200,60,700,NULL,NULL,NULL),(33,'Pintura',300,70,800,NULL,NULL,NULL),(34,'Estuco',400,80,900,NULL,NULL,NULL),(35,'palas',500,90,1000,NULL,NULL,NULL),(36,'Picos',600,100,2000,NULL,NULL,NULL),(37,'Valdosas',700,200,3000,NULL,NULL,NULL),(38,'Maya',800,300,4000,NULL,NULL,NULL),(39,'Ladrillo',900,400,5000,NULL,NULL,NULL),(40,'Carretilla',1000,500,6000,NULL,NULL,NULL),(41,'Cemento',100,50,600,NULL,NULL,NULL),(42,'Vaya',200,60,700,NULL,NULL,NULL),(43,'Pintura',300,70,800,NULL,NULL,NULL),(44,'Estuco',400,80,900,NULL,NULL,NULL),(45,'palas',500,90,1000,NULL,NULL,NULL),(46,'Picos',600,100,2000,NULL,NULL,NULL),(47,'Valdosas',700,200,3000,NULL,NULL,NULL),(48,'Maya',800,300,4000,NULL,NULL,NULL),(49,'Ladrillo',900,400,5000,NULL,NULL,NULL),(50,'Carretilla',1000,500,6000,NULL,NULL,NULL);
/*!40000 ALTER TABLE `material` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proveedor`
--

DROP TABLE IF EXISTS `proveedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proveedor` (
  `IdProveedor` bigint(20) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `TelefonoFijo` bigint(20) DEFAULT NULL,
  `TelefonoCelular` bigint(20) DEFAULT NULL,
  `Direccion` varchar(50) DEFAULT NULL,
  `Correo` varchar(50) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdProveedor`),
  KEY `ProveedorEstado` (`IdEstado`),
  CONSTRAINT `proveedor_ibfk_1` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proveedor`
--

LOCK TABLES `proveedor` WRITE;
/*!40000 ALTER TABLE `proveedor` DISABLE KEYS */;
INSERT INTO `proveedor` VALUES (1,'Brayan Rojas',12345,111222333,'av 34 n°76-45','brayanrojas@gmail.com',NULL),(2,'Luis Lopez',67890,444555666,'av 12 n°76-45','luislopez@gmail.com',NULL),(3,'Anna Quiñones',12345,666777888,'av 23 n°76-45','annaquiñones@gmail.com',NULL),(4,'Sara Bernal',67890,111222333,'av 34 n°76-45','sarabernal@gmail.com',NULL),(5,'Andres Rojas',12345,444555666,'av 45 n°76-45','andresrojas@gmail.com',NULL),(6,'Carlos Ramirez',67890,666777888,'av 56 n°76-45','carlosramirez@gmail.com',NULL),(7,'Gladys Torres',12345,111222333,'av 77 n°76-45','gladystorres@gmail.com',NULL),(8,'Brayan Olivares',67890,444555666,'av 23 n°76-45','brayanolivares@gmail.com',NULL),(9,'Lizeth pardo',12345,666777888,'av 43 n°76-45','lizethpardo@gmail.com',NULL),(10,'Dayana Correa',67890,111222333,'av 54 n°76-45','dayanacorrea@gmail.com',NULL),(11,'Brayan Rojas',12345,111222333,'av 34 n°76-45','brayanrojas@gmail.com',NULL),(12,'Luis Lopez',67890,444555666,'av 12 n°76-45','luislopez@gmail.com',NULL),(13,'Anna Quiñones',12345,666777888,'av 23 n°76-45','annaquiñones@gmail.com',NULL),(14,'Sara Bernal',67890,111222333,'av 34 n°76-45','sarabernal@gmail.com',NULL),(15,'Andres Rojas',12345,444555666,'av 45 n°76-45','andresrojas@gmail.com',NULL),(16,'Carlos Ramirez',67890,666777888,'av 56 n°76-45','carlosramirez@gmail.com',NULL),(17,'Gladys Torres',12345,111222333,'av 77 n°76-45','gladystorres@gmail.com',NULL),(18,'Brayan Olivares',67890,444555666,'av 23 n°76-45','brayanolivares@gmail.com',NULL),(19,'Lizeth pardo',12345,666777888,'av 43 n°76-45','lizethpardo@gmail.com',NULL),(20,'Dayana Correa',67890,111222333,'av 54 n°76-45','dayanacorrea@gmail.com',NULL),(21,'Brayan Rojas',12345,111222333,'av 34 n°76-45','brayanrojas@gmail.com',NULL),(22,'Luis Lopez',67890,444555666,'av 12 n°76-45','luislopez@gmail.com',NULL),(23,'Anna Quiñones',12345,666777888,'av 23 n°76-45','annaquiñones@gmail.com',NULL),(24,'Sara Bernal',67890,111222333,'av 34 n°76-45','sarabernal@gmail.com',NULL),(25,'Andres Rojas',12345,444555666,'av 45 n°76-45','andresrojas@gmail.com',NULL),(26,'Carlos Ramirez',67890,666777888,'av 56 n°76-45','carlosramirez@gmail.com',NULL),(27,'Gladys Torres',12345,111222333,'av 77 n°76-45','gladystorres@gmail.com',NULL),(28,'Brayan Olivares',67890,444555666,'av 23 n°76-45','brayanolivares@gmail.com',NULL),(29,'Lizeth pardo',12345,666777888,'av 43 n°76-45','lizethpardo@gmail.com',NULL),(30,'Dayana Correa',67890,111222333,'av 54 n°76-45','dayanacorrea@gmail.com',NULL),(31,'Brayan Rojas',12345,111222333,'av 34 n°76-45','brayanrojas@gmail.com',NULL),(32,'Luis Lopez',67890,444555666,'av 12 n°76-45','luislopez@gmail.com',NULL),(33,'Anna Quiñones',12345,666777888,'av 23 n°76-45','annaquiñones@gmail.com',NULL),(34,'Sara Bernal',67890,111222333,'av 34 n°76-45','sarabernal@gmail.com',NULL),(35,'Andres Rojas',12345,444555666,'av 45 n°76-45','andresrojas@gmail.com',NULL),(36,'Carlos Ramirez',67890,666777888,'av 56 n°76-45','carlosramirez@gmail.com',NULL),(37,'Gladys Torres',12345,111222333,'av 77 n°76-45','gladystorres@gmail.com',NULL),(38,'Brayan Olivares',67890,444555666,'av 23 n°76-45','brayanolivares@gmail.com',NULL),(39,'Lizeth pardo',12345,666777888,'av 43 n°76-45','lizethpardo@gmail.com',NULL),(40,'Dayana Correa',67890,111222333,'av 54 n°76-45','dayanacorrea@gmail.com',NULL),(41,'Brayan Rojas',12345,111222333,'av 34 n°76-45','brayanrojas@gmail.com',NULL),(42,'Luis Lopez',67890,444555666,'av 12 n°76-45','luislopez@gmail.com',NULL),(43,'Anna Quiñones',12345,666777888,'av 23 n°76-45','annaquiñones@gmail.com',NULL),(44,'Sara Bernal',67890,111222333,'av 34 n°76-45','sarabernal@gmail.com',NULL),(45,'Andres Rojas',12345,444555666,'av 45 n°76-45','andresrojas@gmail.com',NULL),(46,'Carlos Ramirez',67890,666777888,'av 56 n°76-45','carlosramirez@gmail.com',NULL),(47,'Gladys Torres',12345,111222333,'av 77 n°76-45','gladystorres@gmail.com',NULL),(48,'Brayan Olivares',67890,444555666,'av 23 n°76-45','brayanolivares@gmail.com',NULL),(49,'Lizeth pardo',12345,666777888,'av 43 n°76-45','lizethpardo@gmail.com',NULL),(50,'Dayana Correa',67890,111222333,'av 54 n°76-45','dayanacorrea@gmail.com',NULL);
/*!40000 ALTER TABLE `proveedor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `proyecto`
--

DROP TABLE IF EXISTS `proyecto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `proyecto` (
  `IdProyecto` bigint(20) NOT NULL,
  `Nombre` varchar(50) DEFAULT NULL,
  `FechaInicio` date DEFAULT NULL,
  `FechaFinal` date DEFAULT NULL,
  `Presupuesto` bigint(20) DEFAULT NULL,
  `Ciudad` varchar(50) DEFAULT NULL,
  `Cliente` varchar(50) DEFAULT NULL,
  `MaterialUtilizado` varchar(100) DEFAULT NULL,
  `IdEmpleado` bigint(20) DEFAULT NULL,
  `IdEstado` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdProyecto`),
  KEY `empleadopro` (`IdEmpleado`),
  KEY `ProtectoEstado` (`IdEstado`),
  CONSTRAINT `proyecto_ibfk_2` FOREIGN KEY (`IdEstado`) REFERENCES `estado` (`IdEstado`),
  CONSTRAINT `proyecto_ibfk_1` FOREIGN KEY (`IdEmpleado`) REFERENCES `empleado` (`IdEmpleado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `proyecto`
--

LOCK TABLES `proyecto` WRITE;
/*!40000 ALTER TABLE `proyecto` DISABLE KEYS */;
INSERT INTO `proyecto` VALUES (1,'Especto','2016-04-06','2016-04-01',100000,'Bogota','Brayan Rojas','Cemento',NULL,NULL),(2,'Rosal','2016-04-06','2016-04-01',200000,'Medellin','Aangie lorena','Cemento',NULL,NULL),(3,'Centro','2016-04-06','2016-04-01',300000,'bucaramanga','Brayan Olivares','Cemento',NULL,NULL),(4,'Convento','2016-04-06','2016-04-01',400000,'cali','Jason Rodriguez','Cemento',NULL,NULL),(5,'Iglesia','2016-04-06','2016-04-01',500000,'risaralda','Brayan Rojas','Cemento',NULL,NULL),(6,'Casa','2016-04-06','2016-04-01',600000,'choco','Gladys Torres','Cemento',NULL,NULL),(7,'Edificio','2016-04-06','2016-04-01',700000,'manizales','Hector Torres','Cemento',NULL,NULL),(8,'Local','2016-04-06','2016-04-01',800000,'santa marta','Dayana Correa','Cemento',NULL,NULL),(9,'Avenida','2016-04-06','2016-04-01',900000,'barranquilla','Anna Quiñones','Cemento',NULL,NULL),(10,'Poste','2016-04-06','2016-04-01',1000000,'cartagena','Sara Bernal','Cemento',NULL,NULL),(11,'Especto','2016-04-06','2016-04-01',100000,'Bogota','Brayan Rojas','Cemento',NULL,NULL),(12,'Rosal','2016-04-06','2016-04-01',200000,'Medellin','Aangie lorena','Cemento',NULL,NULL),(13,'Centro','2016-04-06','2016-04-01',300000,'bucaramanga','Brayan Olivares','Cemento',NULL,NULL),(14,'Convento','2016-04-06','2016-04-01',400000,'cali','Jason Rodriguez','Cemento',NULL,NULL),(15,'Iglesia','2016-04-06','2016-04-01',500000,'risaralda','Brayan Rojas','Cemento',NULL,NULL),(16,'Casa','2016-04-06','2016-04-01',600000,'choco','Gladys Torres','Cemento',NULL,NULL),(17,'Edificio','2016-04-06','2016-04-01',700000,'manizales','Hector Torres','Cemento',NULL,NULL),(18,'Local','2016-04-06','2016-04-01',800000,'santa marta','Dayana Correa','Cemento',NULL,NULL),(19,'Avenida','2016-04-06','2016-04-01',900000,'barranquilla','Anna Quiñones','Cemento',NULL,NULL),(20,'Poste','2016-04-06','2016-04-01',1000000,'cartagena','Sara Bernal','Cemento',NULL,NULL),(21,'Especto','2016-04-06','2016-04-01',100000,'Bogota','Brayan Rojas','Cemento',NULL,NULL),(22,'Rosal','2016-04-06','2016-04-01',200000,'Medellin','Aangie lorena','Cemento',NULL,NULL),(23,'Centro','2016-04-06','2016-04-01',300000,'bucaramanga','Brayan Olivares','Cemento',NULL,NULL),(24,'Convento','2016-04-06','2016-04-01',400000,'cali','Jason Rodriguez','Cemento',NULL,NULL),(25,'Iglesia','2016-04-06','2016-04-01',500000,'risaralda','Brayan Rojas','Cemento',NULL,NULL),(26,'Casa','2016-04-06','2016-04-01',600000,'choco','Gladys Torres','Cemento',NULL,NULL),(27,'Edificio','2016-04-06','2016-04-01',700000,'manizales','Hector Torres','Cemento',NULL,NULL),(28,'Local','2016-04-06','2016-04-01',800000,'santa marta','Dayana Correa','Cemento',NULL,NULL),(29,'Avenida','2016-04-06','2016-04-01',900000,'barranquilla','Anna Quiñones','Cemento',NULL,NULL),(30,'Poste','2016-04-06','2016-04-01',1000000,'cartagena','Sara Bernal','Cemento',NULL,NULL),(31,'Especto','2016-04-06','2016-04-01',100000,'Bogota','Brayan Rojas','Cemento',NULL,NULL),(32,'Rosal','2016-04-06','2016-04-01',200000,'Medellin','Aangie lorena','Cemento',NULL,NULL),(33,'Centro','2016-04-06','2016-04-01',300000,'bucaramanga','Brayan Olivares','Cemento',NULL,NULL),(34,'Convento','2016-04-06','2016-04-01',400000,'cali','Jason Rodriguez','Cemento',NULL,NULL),(35,'Iglesia','2016-04-06','2016-04-01',500000,'risaralda','Brayan Rojas','Cemento',NULL,NULL),(36,'Casa','2016-04-06','2016-04-01',600000,'choco','Gladys Torres','Cemento',NULL,NULL),(37,'Edificio','2016-04-06','2016-04-01',700000,'manizales','Hector Torres','Cemento',NULL,NULL),(38,'Local','2016-04-06','2016-04-01',800000,'santa marta','Dayana Correa','Cemento',NULL,NULL),(39,'Avenida','2016-04-06','2016-04-01',900000,'barranquilla','Anna Quiñones','Cemento',NULL,NULL),(40,'Poste','2016-04-06','2016-04-01',1000000,'cartagena','Sara Bernal','Cemento',NULL,NULL),(41,'Especto','2016-04-06','2016-04-01',100000,'Bogota','Brayan Rojas','Cemento',NULL,NULL),(42,'Rosal','2016-04-06','2016-04-01',200000,'Medellin','Aangie lorena','Cemento',NULL,NULL),(43,'Centro','2016-04-06','2016-04-01',300000,'bucaramanga','Brayan Olivares','Cemento',NULL,NULL),(44,'Convento','2016-04-06','2016-04-01',400000,'cali','Jason Rodriguez','Cemento',NULL,NULL),(45,'Iglesia','2016-04-06','2016-04-01',500000,'risaralda','Brayan Rojas','Cemento',NULL,NULL),(46,'Casa','2016-04-06','2016-04-01',600000,'choco','Gladys Torres','Cemento',NULL,NULL),(47,'Edificio','2016-04-06','2016-04-01',700000,'manizales','Hector Torres','Cemento',NULL,NULL),(48,'Local','2016-04-06','2016-04-01',800000,'santa marta','Dayana Correa','Cemento',NULL,NULL),(49,'Avenida','2016-04-06','2016-04-01',900000,'barranquilla','Anna Quiñones','Cemento',NULL,NULL),(50,'Poste','2016-04-06','2016-04-01',1000000,'cartagena','Sara Bernal','Cemento',NULL,NULL);
/*!40000 ALTER TABLE `proyecto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo`
--

DROP TABLE IF EXISTS `tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo` (
  `IdTipo` bigint(20) NOT NULL,
  `Costo` bigint(20) DEFAULT NULL,
  `IdProyecto` bigint(20) DEFAULT NULL,
  `IdMaterial` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`IdTipo`),
  KEY `materialtipo` (`IdMaterial`),
  KEY `tipopro` (`IdProyecto`),
  CONSTRAINT `tipo_ibfk_2` FOREIGN KEY (`IdMaterial`) REFERENCES `material` (`IdMaterial`),
  CONSTRAINT `tipo_ibfk_3` FOREIGN KEY (`IdProyecto`) REFERENCES `proyecto` (`IdProyecto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo`
--

LOCK TABLES `tipo` WRITE;
/*!40000 ALTER TABLE `tipo` DISABLE KEYS */;
INSERT INTO `tipo` VALUES (1,100000,NULL,NULL),(2,200000,NULL,NULL),(3,300000,NULL,NULL),(4,400000,NULL,NULL),(5,500000,NULL,NULL),(6,600000,NULL,NULL),(7,700000,NULL,NULL),(8,800000,NULL,NULL),(9,900000,NULL,NULL),(10,1000000,NULL,NULL),(11,100000,NULL,NULL),(12,200000,NULL,NULL),(13,300000,NULL,NULL),(14,400000,NULL,NULL),(15,500000,NULL,NULL),(16,600000,NULL,NULL),(17,700000,NULL,NULL),(18,800000,NULL,NULL),(19,900000,NULL,NULL),(20,1000000,NULL,NULL),(21,100000,NULL,NULL),(22,200000,NULL,NULL),(23,300000,NULL,NULL),(24,400000,NULL,NULL),(25,500000,NULL,NULL),(26,600000,NULL,NULL),(27,700000,NULL,NULL),(28,800000,NULL,NULL),(29,900000,NULL,NULL),(30,1000000,NULL,NULL),(31,100000,NULL,NULL),(32,200000,NULL,NULL),(33,300000,NULL,NULL),(34,400000,NULL,NULL),(35,500000,NULL,NULL),(36,600000,NULL,NULL),(37,700000,NULL,NULL),(38,800000,NULL,NULL),(39,900000,NULL,NULL),(40,1000000,NULL,NULL),(41,100000,NULL,NULL),(42,200000,NULL,NULL),(43,300000,NULL,NULL),(44,400000,NULL,NULL),(45,500000,NULL,NULL),(46,600000,NULL,NULL),(47,700000,NULL,NULL),(48,800000,NULL,NULL),(49,900000,NULL,NULL),(50,1000000,NULL,NULL);
/*!40000 ALTER TABLE `tipo` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-18 12:30:30
